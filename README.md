## Курсовая работа по итогам модуля "DevOps и системное администрирование"

* Процесс установки и настройки ufw
  * Установка пакета ufw 
  ```bash
  root@vault:~# aptitude install ufw
  ```
  * Установка правил для фаервола пропускать трафик ssh, https и весть трафик для localhost
  ```bash 
  root@vault:~# ufw allow ssh
  root@vault:~# ufw allow https
  root@vault:~# ufw allow from 127.0.0.1
  ```
  * Включение фаервола и проверка его работы
  ```bash
  root@vault:~# ufw enable
  root@vault:~# ufw status
  Status: active

  To                         Action      From
  --                         ------      ----
  22/tcp                     ALLOW       Anywhere
  443                        ALLOW       Anywhere
  Anywhere                   ALLOW       127.0.0.1
  22/tcp (v6)                ALLOW       Anywhere (v6)
  443 (v6)                   ALLOW       Anywhere (v6)
  ```
* Процесс установки и выпуска сертификата с помощью hashicorp vault

  * Установка пакетов которые нам понадобятся в процессе запуска сервиса
  ```bash
  root@vault:~# aptitude install gnupg2 curl lsb-release software-properties-common jq
  ```
  * Установка hashicorp vault
  ```bash
  root@vault:~# curl -fsSL https://apt.releases.hashicorp.com/gpg -x 127.0.0.1:3128 | sudo apt-key add -
  root@vault:~# aptitude update
  root@vault:~# aptitude install vault  
  ```
  * Запуск сервиса и добавление переменных окружения 
  ```bash
  root@vault:~# vault server -dev -dev-root-token-id root
  barrymore@vault:~$ export VAULT_ADDR=http://127.0.0.1:8200
  barrymore@vault:~$ export VAULT_TOKEN=root
  ```
  * Генерация корневого сертификата
  ```bash
  barrymore@vault:~$ vault secrets enable pki
  barrymore@vault:~$ vault secrets tune -max-lease-ttl=720h pki
  barrymore@vault:~$ vault write -field=certificate pki/root/generate/internal \ common_name="example.com" \ ttl=87600h > CA_cert.crt
  barrymore@vault:~$ vault write pki/config/urls \ issuing_certificates="$VAULT_ADDR/v1/pki/ca" \ crl_distribution_points="$VAULT_ADDR/v1/pki/crl"
  ```
  * Генерация промежуточного сертификата
  ```bash
  barrymore@vault:~$ vault secrets enable -path=pki_int pki
  barrymore@vault:~$ vault secrets tune -max-lease-ttl=43800h pki_int
  barrymore@vault:~$ vault write -format=json pki_int/intermediate/generate/internal      common_name="example.com Intermediate Authority"      | jq -r '.data.csr' > pki_intermediate.csr
  barrymore@vault:~$ vault write -format=json pki/root/sign-intermediate csr=@pki_intermediate.csr      format=pem_bundle ttl="43800h"      | jq -r '.data.certificate' > intermediate.cert.pem
  barrymore@vault:~$ vault write pki_int/intermediate/set-signed certificate=@intermediate.cert.pem
  ```
  * Создание роли для выпуска сертификатов
  ```bash
  barrymore@vault:~$ vault write pki_int/roles/example-dot-com \ allowed_domains="example.com" \ allow_subdomains=true \ max_ttl="720h"
  ```
  * Создание сертифика сервера
  ```bash
  barrymore@vault:~$ vault write pki_int/issue/example-dot-com common_name="wpbase.example.com" ttl="30d"
  ```
* Процесс установки и настройки сервера nginx
    
  * Установка nginx на хост
  ```bash
  barrymore@wpbase:~$ sudo aptitude install nginx
  ```
  * Удаляем стандартный конфиг nginx
  ```bash
  barrymore@wpbase:~$ sudo rm /etc/nginx/sites-enabled/default
  ```
  * Пишем свой конфиг для nginx под сайт на wordpress, в рамках данной курсовой работы не требуется расписывать процес его установки поэтому этот момент пропустим
  ```bash
  barrymore@wpbase:~$ sudo nano /etc/nginx/sites-enabled/wpbase.conf
  ```
  * Содержание файла `wpbase.conf` путь к сертификату и ключу указаны в параметрах `ssl_certificate` и `ssl_certificate_key`, также добавлен редирект с http на https
  ```
  server {
        listen 80 default_server;
        server_name wpbase.example.com;
        return 301 https://$host$request_uri;
  }

  server {
        listen 443 ssl;
        root /var/www/html/wpbase;
        index  index.php index.html index.htm;
        server_name wpbase.example.com;

        error_log /var/log/nginx/wpbase_error.log;
        access_log /var/log/nginx/wpbase_access.log;

        ssl_certificate /etc/ssl/certs/wpbase.example.com.pem;
        ssl_certificate_key /etc/ssl/private/wpbase.example.com.key;

        client_max_body_size 100M;
        location / {
                try_files $uri $uri/ /index.php?$args;
        }
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }
  }
  ```
* Страница сайта

  ![](img/cert.PNG)
* Скрипт генерации нового сертификата, как вариант реализации я выбрал метод когда хост на котором нужно обновить сертифика обращается к серверу vault через api по средствам curl запроса это позволит разворачивать и запускать скрипт на нескольких машинах сразу и такой сценари более приближен к "боевому", разумеется для этого потом потребуется защитить сервис используя шифрованые токены и порт с api доступный только для определенных хостов
  * Скрипт
  ```
  #/usr/bin/env bash

  REQUIRED_PKG=("jq" "curl" "net-tools")
  VAULT_TOKEN=root
  VAULT_ADDR=https://172.21.5.122
  TTL=30d
  FQDN="${HOSTNAME}.example.com"
  ISSUER=example-dot-com
  CERT_DIR="/etc/ssl/certs/"
  KEY_DIR="/etc/ssl/private/"
  HEADER='"X-Vault-Token: '$VAULT_TOKEN'"'
  DATA='{"common_name": "'${FQDN}'", "ttl": "'${TTL}'"}'
  DATA="'"$DATA"'"
  CURL="curl --header $HEADER --request POST --data $DATA $VAULT_ADDR/v1/pki_int/issue/$ISSUER"
  
  # check if required packages are installed
  for i in ${REQUIRED_PKG[@]}
  do
    echo Checking for $i
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $i|grep "install ok installed")
    if [ "" = "$PKG_OK" ]; then
      PKG_TO_INST="$PKG_TO_INST$i "
    fi
  done
  echo $PKG_TO_INST
  if [ -n "$PKG_TO_INST" ]; then
    echo "No $PKG_TO_INST. Setting up $PKG_TO_INST."
    sudo apt-get update
    sudo apt-get -y install $PKG_TO_INST
  else
    echo "All requested packages already installed"
  fi
  
  #Get cert from server into file
  eval $CURL > $FQDN.full
  
  # Parsing certificates
  cat $FQDN.full | jq -r .data.certificate > $FQDN.pem
  cat $FQDN.full | jq -r .data.issuing_ca|tee -a $FQDN.pem
  cat $FQDN.full | jq -r .data.private_key > $FQDN.key
  
  # Check if curl and parsing worked right
  if [ -s $FQDN.pem ] && [ -s $FQDN.key ];
  then
    echo "Cert files ok"
  else
    echo "Empty cert files exit"
    exit 1
  fi
  
  # Moving files to folders
  sudo mv $FQDN.pem $CERT_DIR
  sudo mv $FQDN.key $KEY_DIR
  
  # Test nginx config and restart service
  if sudo nginx -t 2>/dev/null;
  then
    sudo service nginx restart
  else
    echo 'An error occurred while testing nginx config'
    exit 1
  fi

  #Cleanup after run
  rm $FQDN.full
  
  ```
  * Вывод из терминала
  ```bash
  barrymore@wpbase:~$ date
  Wed Dec 29 16:21:41 MSK 2021
  barrymore@wpbase:~$ ./cert_update.sh
  Checking for jq
  Checking for curl
  Checking for net-tools
  
  All requested packages already installed
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                   Dload  Upload   Total   Spent    Left  Speed
  100  6059    0  6008  100    51  21768    184 --:--:-- --:--:-- --:--:-- 21952
  -----BEGIN CERTIFICATE-----
  MIIDpjCCAo6gAwIBAgIUaVWHFEeAQsnHCov6JHYEDJdGmzIwDQYJKoZIhvcNAQEL
  BQAwFjEUMBIGA1UEAxMLZXhhbXBsZS5jb20wHhcNMjExMjI4MDYzNTI3WhcNMjYx
  MjI3MDYzNTU3WjAtMSswKQYDVQQDEyJleGFtcGxlLmNvbSBJbnRlcm1lZGlhdGUg
  QXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnxKpZxeM
  ARKxi2ZTJY6EaDArVn0BotzCxCyGDaEdz3J/HY+dNMR6t1jpbbLE5PJVTEAcCtMa
  pPhxMkTcYT1/gsNAFOv6ik+rXjm3UTVk666wuWZzYdkcofkU2Z/I5pf+UbJHCsaT
  dZzc0f2M28uUah8K1VfiJ6+ICZUUyxTDmkbJr1vSzAXg/T3tPgtn/EwmKRj8sPWx
  hnnt7gEfTdMVu8a4cDeytIgJIYpeRwDB2dO2yWNI8NEKSfzqljqajkckrBPfGtGQ
  9D/VbjQnQJdnvByiVCGBAXy6/WKEfFuDWn5C6LpygSYekuj0O0RuU1Gq9J+w+vzv
  vSVfgzOCrtBCHwIDAQABo4HUMIHRMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8E
  BTADAQH/MB0GA1UdDgQWBBQxYGMR5eweXoaxsmwfwVJpdf5l+TAfBgNVHSMEGDAW
  gBTj6+IrHmO/+BKIoI+w7NnyHznyKTA7BggrBgEFBQcBAQQvMC0wKwYIKwYBBQUH
  MAKGH2h0dHA6Ly8xMjcuMC4wLjE6ODIwMC92MS9wa2kvY2EwMQYDVR0fBCowKDAm
  oCSgIoYgaHR0cDovLzEyNy4wLjAuMTo4MjAwL3YxL3BraS9jcmwwDQYJKoZIhvcN
  AQELBQADggEBAMfOK+VkYc9IlgwzS/xe5+/kkA0ISImIPsaXTs5cVNpVNRtzIygh
  dVFkB5cQkhPv45lphSNrdoeeU6ewjNXw7QZSLFIaj8NvqFyOs9Vi3sT8TFzU9Qya
  AR8ewPt04YypKiR+l3ekYDxlVF72E5B15sZwlE3E10Q3QvggqytiDi8RY72VgXea
  9kSgtzSfB30eIjeS9pjfS0VqWuuiJyAYzkDvdVVSaEDR/QM4/Bi/t64Qy/hD0qI7
  lcJtuNwtN0daQMd2T02RkD5GNAVo8T3y2hMm91U8yUIL5DxlHRo3BfYuLlfdnDEH
  BR8Q7nwuo5SxbSRlV12R5nfTRAR6p0+bNr4=
  -----END CERTIFICATE-----
  Cert files ok
  ```
  * В браузере

  ![](img/cert_sh.PNG)
* Crontab
  * Задача в Crontab обновление сертификата в 17:08 15 и 29 числа каждого месяца 
  ```
  8 17 15,29 * * /bin/bash /home/barrymore/cert_update.sh
  ```
  * Скриншот из браузера

  ![](img/cert_cron.PNG)
