#/usr/bin/env bash

REQUIRED_PKG=("jq" "curl" "net-tools")
VAULT_TOKEN=root
VAULT_ADDR=https://172.21.5.122
TTL=30d
FQDN="${HOSTNAME}.example.com"
ISSUER=example-dot-com
CERT_DIR="/etc/ssl/certs/"
KEY_DIR="/etc/ssl/private/"
HEADER='"X-Vault-Token: '$VAULT_TOKEN'"'
DATA='{"common_name": "'${FQDN}'", "ttl": "'${TTL}'"}'
DATA="'"$DATA"'"
CURL="curl --header $HEADER --request POST --data $DATA $VAULT_ADDR/v1/pki_int/issue/$ISSUER"

# check if required packages are installed
for i in ${REQUIRED_PKG[@]}
do
  echo Checking for $i
  PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $i|grep "install ok installed")
  if [ "" = "$PKG_OK" ]; then
    PKG_TO_INST="$PKG_TO_INST$i "
  fi
done
echo $PKG_TO_INST
if [ -n "$PKG_TO_INST" ]; then
  echo "No $PKG_TO_INST. Setting up $PKG_TO_INST."
  sudo apt-get update
  sudo apt-get -y install $PKG_TO_INST
else
  echo "All requested packages already installed"
fi

#Get cert from server into file
eval $CURL > $FQDN.full

# Parsing certificates
cat $FQDN.full | jq -r .data.certificate > $FQDN.pem
cat $FQDN.full | jq -r .data.issuing_ca|tee -a $FQDN.pem
cat $FQDN.full | jq -r .data.private_key > $FQDN.key

# Check if curl and parsing worked right
if [ -s $FQDN.pem ] && [ -s $FQDN.key ];
then
  echo "Cert files ok"
else
  echo "Empty cert files exit"
  exit 1
fi

# Moving files to folders
sudo mv $FQDN.pem $CERT_DIR
sudo mv $FQDN.key $KEY_DIR

# Test nginx config and restart service
if sudo nginx -t 2>/dev/null;
then
  sudo service nginx restart
else
  echo 'An error occurred while testing nginx config'
  exit 1
fi

#Cleanup after run
rm $FQDN.full
